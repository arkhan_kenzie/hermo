<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return view('home');
});
*/

Route::get('/home', function () {
    return view('home2');
});

Route::get('getDataJSONHermo', 'JsonController@getDataJSONHermo');
Route::get('/', 'HomeController@index');
Route::get('detail_item', 'HomeController@getDetailItem');

//backend
Route::get('/backend', 'HomeController@backend');
Route::get('detail_item_backend', 'HomeController@getDetailItemBackend');
Route::post('/backend/purchase/{id?}', 'HomeController@purchase');
//Route::get('/backend/purchase/{id?}', 'HomeController@purchase');
Route::post('/backend/order', 'FrontendController@order');


