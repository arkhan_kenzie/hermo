<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FrontendController extends Controller
{
    public function __construct(){
		
	}
	
	public function order(Request $request)
	{
		$receiver = $_POST['receiver'];
		$brand = $_POST['brand'];
		$images = $_POST['images'];
		$name = $_POST['name'];
		$symbol = $_POST['symbol'];
		$retail_price = $_POST['retail_price'];
		$selling_price = $_POST['selling_price'];
		$qty = $_POST['qty'];
		$amount = $_POST['amount'];
		$promotion_code = $_POST['promotion_code'];
		if ($promotion_code<>'OFF5PC' || $promotion_code=='GIVEME15'){
			//return Redirect::back()->withInput()->withErrors("promotion code is invalid");
			echo "promotion code is invalid";
			exit;
		}
		
		//rules of discount
		$discount = 0;
		if ($promotion_code=='OFF5PC' && $qty=='2'){
			$discount = $amount * (5/100);
		}
		if ($promotion_code=='GIVEME15' && $amount>=100){
			$discount = $amount - 15;
		}
		
		if ($receiver=='Malaysia'){
			$shipping_fee = 10;
			if ($qty==2 || $amount>150){
				$shipping_fee = 0;
			}
		}
		if ($receiver=='Singapore'){
			$shipping_fee = 20;
			if ($amount>300){
				$shipping_fee = 0;
			}
		}
		if ($receiver=='Brunei'){
			$shipping_fee = 25;
			if ($amount>300){
				$shipping_fee = 0;
			}
		}
		
		$payment = ($amount-$discount)+ $shipping_fee ;
		
		$data = array('brand'=>$brand,'images'=>$images,'name'=>$name,'retail_price'=>$retail_price,
					'selling_price'=>$selling_price,'amount'=>$amount,'promotion_code'=>$promotion_code,
					'symbol'=>$symbol,'qty'=>$qty,'discount'=>$discount,'shipping_fee'=>$shipping_fee,
					'payment'=>$payment,'receiver'=>$receiver);
		
		return view('backend.order', $data);
	}
}
