<?php namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\User;

use DB;
use Response;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$url = 'https://www.hermo.my/test/api.html?list=banner';
		$json = file_get_contents($url);
		$banner = json_decode($json,true);					
		
		$url = 'https://www.hermo.my/test/api.html?list=best-selling';
		$json = file_get_contents($url);
		$best_selling = json_decode($json,true);					
		
		$data = array('best_selling'=>$best_selling,'banner'=>$banner);
		return view('home',$data);
	}
	
	/*
	public function array_sort($array, $type='asc'){
		$result=array();
		foreach($array as $var => $val){
			$set=false;
			foreach($result as $var2 => $val2){
				if($set==false){
					if($val>$val2 && $type=='desc' || $val<$val2 && $type=='asc'){
						$temp=array();
						foreach($result as $var3 => $val3){
							if($var3==$var2) $set=true;
							if($set){
								$temp[$var3]=$val3;
								unset($result[$var3]);
							}
						}
						$result[$var]=$val;    
						foreach($temp as $var3 => $val3){
							$result[$var3]=$val3;
						}
					}
				}
			}
			if(!$set){
				$result[$var]=$val;
			}
		}
		return $result;
	} */
	
	function order_array_num ($array, $key, $order = "ASC") 
    { 
        $tmp = array(); 
        foreach($array as $akey => $array2) 
        { 
            $tmp[$akey] = $array2[$key]; 
        } 
        
        if($order == "DESC") 
			{arsort($tmp , SORT_NUMERIC );} 
        else 
			{asort($tmp , SORT_NUMERIC );} 

        $tmp2 = array();        
        foreach($tmp as $key => $value) 
        { 
            $tmp2[$key] = $array[$key]; 
        }        
        
        return $tmp2; 
    } 
	
	public function array_sort($array, $on, $order=SORT_ASC) {
		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
				break;
				case SORT_DESC:
					arsort($sortable_array);
				break;
			}

			foreach ($sortable_array as $k => $v) {
				array_push($new_array, $array[$k]);
			}
		}

		return $new_array;
	}

	public function sortBy($field, &$array, $direction = 'asc')
	{
		usort($array, create_function('$a, $b', '
			$a = $a["' . $field . '"];
			$b = $b["' . $field . '"];

			if ($a == $b)
			{
				return 0;
			}

			return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
		'));

		return true;
	}
	
	public function build_sorter($key) {
		return function ($a, $b) use ($key) {
			return strnatcmp($a[$key], $b[$key]);
		};
	}

	public function getDetailItem()
	{
		//$url = 'https://www.hermo.my/test/api.html?list=best-selling';
		//$array = file_get_contents($url);
		//$array = json_decode($array,true);		
		//echo print_r($array);exit;
		//usort($array, $this->build_sorter('selling_price'));
		//exit;
		
		$url = 'https://www.hermo.my/test/api.html?list=best-selling';
		$json = file_get_contents($url);
		$best_selling = json_decode($json,true);					
		$data = array('best_selling'=>$best_selling);
		return view('detail_item',$data);
		
		/*
		usort($best_selling, function($a, $b) { //Sort the array using a user defined function
			return $a->selling_price > $b->selling_price ? -1 : 1; //Compare the scores
		});                                                                                                                                                                                                        
		*/
		
		//$this->array_sort ($best_selling,"desc");
		//$this->order_array_num($best_selling,"selling_price","DESC");
		
		//$best_selling = ksort($best_selling);
		//$best_selling = $this->sortBy("selling_price",$best_selling);
		//echo 'ssss';exit;
		//$best_selling = array_reverse($best_selling);
		//print_r($best_selling);  exit;
		//foreach($age as $x => $x_value) {
		//echo "Key=" . $x . ", Value=" . $x_value;
		//echo "<br>";
	
		//$best_selling = json_encode($best_selling,true);					
		//print_r($this->array_sort($best_selling, 'name', SORT_DESC)); // 
		//print_r($this->array_sort($best_selling, 'name', SORT_ASC)); // 
		//print_r(array_sort($people, 'surname', SORT_ASC)); // Sort by surname
		//exit;
		
		//$best_selling = json_decode($best_selling,true);	
		//$data = $best_selling;		
		
		/*
		$col = new \Illuminate\Database\Eloquent\Collection();
		foreach($json->get() as $im) {
			$col->add($im);    
		} */
		//$col = $col->sortBy('created_at');
		//echo print_r($best_selling);
		//echo print_r($data);exit;
		
		//$file = file_get_contents('books.json');
		
		/*
		
		$json = json_decode($url, true);
		//$json = array_map(function($a) { return $a['items'][0]; });
		//$json = array_map(function($a) { return $a['items'][0]; }, $json['items']);
		foreach ($json as $key => $row) {
			$title[$key] = $row['best_selling'];
		   $author[$key] = $row['name'];
		}
		array_multisort($title, SORT_ASC,  $author, SORT_DESC, $json);
		echo "<pre>";
		print_r($json);
		echo "</pre>";
		
		/////
		$sort = array();
		foreach($json['books'] as $k=>$v) {
		$sort['title'][$k] = $v['book'][0]['Title'];
		$sort['author'][$k] = $v['book'][0]['Author'];
		}
		//print_r($sort);
		array_multisort($sort['title'], SORT_DESC, $sort['author'], SORT_ASC,$json['books']);
		print_r($json['books']);
		
		*/
	}
	
	public function backend()
	{
		$url = 'https://www.hermo.my/test/api.html?list=banner';
		$json = file_get_contents($url);
		$banner = json_decode($json,true);					

		$url = 'https://www.hermo.my/test/api.html?list=best-selling';
		$json = file_get_contents($url);
		$best_selling = json_decode($json,true);		
		
		$data = array('best_selling'=>$best_selling,'banner'=>$banner);
		return view('backend',$data);
	}	
	
	public function getDetailItemBackend()
	{
		$url = 'https://www.hermo.my/test/api.html?list=best-selling';
		$json = file_get_contents($url);
		$best_selling = json_decode($json,true);					
		$data = array('best_selling'=>$best_selling);
		return view('detail_item_back',$data);
	}
	
	public function purchase($id)
	{
		$url = 'https://www.hermo.my/test/api.html?list=best-selling';
		$json = file_get_contents($url);
		$best_selling = json_decode($json);	
		//var_dump($best_selling);exit;

		foreach($best_selling->items as $item)
		{
			if($item->id == $id)
			{
				$images = $item->images;
				$brand = $item->brand->name;
				$name = $item->name;
				$symbol= $item->symbol;
				$selling_price = $item->selling_price;
				$retail_price = $item->retail_price;
				
			}
		}
		 
		$data = array('best_selling'=>$best_selling,'brand'=>$brand, 'name'=>$name,'selling_price'=>$selling_price,'images'=>$images,'retail_price'=>$retail_price,'symbol'=>$symbol);
		return view('backend/purchase',$data);
	}
}
