<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Session;

use App\Mobil; 				//memanggil model mobil
use App\Comment; 			//memanggil model comment
use App\Models\GroupUser;  //memanggil model group user
use App\Models\User;  		//memanggil model user
use App\Models\Kecamatan;  		//memanggil model kecamatan
use App\Models\Jalan; 		//Model Jalan
use App\Models\Kondisi;		//Model Kondisi
use App\Models\TipeBahan; 	//Model tipe bahan
use App\Models\Penanganan;	//Model Penanganan

use App\Models\Jembatan; 		//memanggil model jembatan

use Datatables; //memanggil datatables
use Response;

use DB;

class DatatablesController extends Controller {

	public function getDataMobil()
    {
		/*
		$mobil_DT = Mobil::select(array(
            'id','nopol','merk_mobil'
        ));
		*/
		$mobil = Mobil::all();
		return Datatables::of($mobil)->make(true);
		
		return Response::json([
			'data'=>$mobil
		],200);
		
        //$mobil_DT = Mobil::select(['id','nopol','merk_mobil'])->get();
		//$mobil_DT = Mobil::all();
        //return Datatables::of($mobil_DT::query())->make(true);
    }
		
	public function getDataGroupUser()
    {
		$group_user = GroupUser::all();
		return Datatables::of($group_user)->make(true);
    }
	
	public function getDataUser()
    {
		$user = User::all();
		return Datatables::of($user)->make(true);
    }
	
	public function getDataKecamatan()
    {
		$kecamatan = Kecamatan::select('id','pkkecode','nama_kec','published')->orderBy('id','desc')->get();
		return Datatables::of($kecamatan)->make(true);
    }
	
	public function getDataJalan()
    {
		$jalan = Jalan::select('id','nama_jalan','published')->orderBy('id','desc')->get();
		return Datatables::of($jalan)->make(true);
    }
	
	public function getDataKondisi()
    {
		$kondisi = Kondisi::select('id','kondisi','published')->orderBy('id','desc')->get();
		return Datatables::of($kondisi)->make(true);
    }
	
	public function getDataTipeBahan()
    {
		$tipe_bahan = TipeBahan::select('id','tipe_bahan','published')->orderBy('id','desc')->get();
		return Datatables::of($tipe_bahan)->make(true);
    }
	
	public function getDataPenanganan() //Membaca data jembatan
    {
		$penanganan = Jembatan::select('id','nama_jembatan','nama_jalan','published')->orderBy('id','desc')->get();
		return Datatables::of($penanganan)->make(true);
    }
	
	public function getDataPenangananDetail() //transaksi penanganan jembatan
    {
		$id = $_GET['id'];
		$penanganan = Penanganan::select('tb_penanganan_jembatan.id','bagian','tb_tindakan_penanganan.jenis_tindakan','tanggal','tb_status_penanganan.status_penanganan','tb_penanganan_jembatan.published')
						->where('id_jembatan',$id)
						->join('tb_tindakan_penanganan','tb_penanganan_jembatan.tindakan','=','tb_tindakan_penanganan.id')
						->join('tb_status_penanganan','tb_penanganan_jembatan.status_penanganan','=','tb_status_penanganan.id')
						->orderBy('tanggal','desc')->get();
						
		return Datatables::of($penanganan)->make(true);
    }
	
	public function getDataInfoPenanganan($id) //Membaca data jembatan yang ada penanganannya
    {
		//$penanganan = Jembatan::select('id','nama_jembatan','nama_jalan','published')->orderBy('id','desc')->get();
		//$id = $_GET['id'];
		$penanganan = Jembatan::select('tb_penanganan_jembatan.id','nama_jembatan','nama_jalan','tb_kecamatan.nama_kec','tb_desa.desaname',
					 'tb_penanganan_jembatan.bagian','tb_penanganan_jembatan.tindakan','jenis_tindakan','tb_penanganan_jembatan.keterangan','tb_status_penanganan.status_penanganan','tb_jembatan.published')
					->where('id_jembatan',$id)
					->join('tb_kecamatan','tb_kecamatan.pkkecode','=','tb_jembatan.kecamatan')
					->join('tb_desa','tb_desa.admncode','=','tb_jembatan.desa')
					->join('tb_penanganan_jembatan','tb_penanganan_jembatan.id_jembatan','=','tb_jembatan.id')
					->join('tb_tindakan_penanganan','tb_tindakan_penanganan.id','=','tb_penanganan_jembatan.tindakan')
					->join('tb_status_penanganan','tb_status_penanganan.id','=','tb_penanganan_jembatan.status_penanganan');
		return Datatables::of($penanganan)->make(true);
    }
	
	public function getDataJembatan()
    {
		$jembatan = Jembatan::select('tb_jembatan.id','tb_jembatan.nama_jembatan','tb_jembatan.nama_jalan','tb_kecamatan.nama_kec','tb_desa.desaname','tb_jembatan.panjang','tb_jembatan.lebar','tb_jembatan.published')
					->join('tb_kecamatan','tb_kecamatan.pkkecode','=','tb_jembatan.kecamatan')
					->join('tb_desa','tb_desa.admncode','=','tb_jembatan.desa');
		//			//->orderBy('tb_jembatan.id','desc')->get();
		//$jembatan = DB::select("SELECT tb_jembatan.id, nama_jembatan,nama_jalan, tb_kecamatan.nama_kec, tb_desa.desaname, panjang, lebar, tb_jembatan.published
		//							  FROM tb_jembatan JOIN tb_kecamatan ON tb_kecamatan.pkkecode = tb_jembatan.kecamatan
		//							  JOIN tb_desa ON tb_desa.admncode = tb_jembatan.desa");

		return Datatables::of($jembatan)->make(true);
    }
	
	public function getDataInfoPenangananDetail($id) //Membaca data jembatan yang ada penanganannya scr details
    {
		$penanganan = Jembatan::select('tb_penanganan_jembatan.id','nama_jembatan','nama_jalan','tb_kecamatan.nama_kec','tb_desa.desaname',
					 'tb_penanganan_jembatan.bagian','tb_penanganan_jembatan.tindakan','jenis_tindakan','tb_penanganan_jembatan.keterangan','tb_jembatan.published')
					//->where('id_jembatan',$id)
					->join('tb_kecamatan','tb_kecamatan.pkkecode','=','tb_jembatan.kecamatan')
					->join('tb_desa','tb_desa.admncode','=','tb_jembatan.desa')
					->join('tb_penanganan_jembatan','tb_penanganan_jembatan.id_jembatan','=','tb_jembatan.id')
					->join('tb_tindakan_penanganan','tb_tindakan_penanganan.id','=','tb_penanganan_jembatan.tindakan');
		return Response::json($penanganan);
    }

	//GET DATA GIS
	public function getDataGISJalan()
    {		
		$id = "";
		if (ISSET($_GET['jalan']))
		{
			$id = $_GET['jalan'];
			$jalan = DB::select("SELECT id, nama_jalan, polyline FROM tb_jalan WHERE id='$id' AND (polyline<>'' OR polyline IS NOT NULL)");
		}
		else
			$jalan = DB::select("SELECT id, nama_jalan, polyline FROM tb_jalan WHERE polyline<>'' OR polyline IS NOT NULL");
		
		//echo print_r($jalan);exit;
		$geoJSON = array(
			'type' => 'featureCollection',
			'features' => array()
		);
		
								
		foreach($jalan as $row){
			$polyline = explode(",",$row->polyline);
			foreach($polyline as $key => $val) {
				 $polyline[$key] = ltrim($polyline[$key]," ");
				 $polyline[$key] = "[" . $polyline[$key] . "]";
			}
			$polyline = str_replace(" ", "," ,$polyline);
			$poly = "";
			for ($i=0;$i<count($polyline);$i++){
				if ($i==count($polyline)-1)
					$poly = $poly.$polyline[$i];
				else
					$poly = $poly.$polyline[$i].",";
			}
			
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'id' 				=> $row->id,									
					'nama_jalan' 		=> $row->nama_jalan,									
					'polyline' 			=> $row->polyline
				),
				'geometry' => array(
					'type' => 'LineString',
					'coordinates' => $poly
					
				)
			);
			array_push($geoJSON['features'], $marker);	
		}
		header('Content-type: application/json');
		$hasil = json_encode($geoJSON,JSON_NUMERIC_CHECK);
		$hasil = str_replace('"[','[[',$hasil);
		$hasil = str_replace(']"',']]',$hasil);
		echo $hasil;
		//echo json_encode(str_replace('"[','[',$geoJSON),JSON_NUMERIC_CHECK);
    }
	
	public function getDataGISKecamatan()
    {		
		$kec = "";
		if (ISSET($_GET['kec']))
		{
			$kec = $_GET['kec'];
			$kecamatan = DB::select("SELECT id, pkkecode, nama_kec, ST_AsText(ST_Transform(geom, 4326)) as polygon  FROM tb_kecamatan WHERE pkkecode='$kec'");
		}
		else
			$kecamatan = DB::select("SELECT id, pkkecode, nama_kec, ST_AsText(ST_Transform(geom, 4326)) as polygon  FROM tb_kecamatan");
			
		$geoJSON = array(
			'type' => 'featureCollection',
			'features' => array()
		);
		
								
		foreach($kecamatan as $row){
			$polygon = str_replace("MULTIPOLYGON(((","",$row->polygon);
			$polygon = str_replace(")))","",$polygon);
			$polygon = explode(",",$polygon);
			foreach($polygon as $key => $val) {
				 $polygon[$key] = ltrim($polygon[$key]," ");
				 $polygon[$key] = "[" . $polygon[$key] . "]";
				 //echo $polygon[$key] ;echo "<br />";
			}
			$polygon = str_replace(" ", "," ,$polygon);
			//echo $polygon[0];exit;
			
			$poly = "";
			for ($i=0;$i<count($polygon);$i++){
				if ($i==count($polygon)-1)
					$poly = $poly.$polygon[$i];
				else
					$poly = $poly.$polygon[$i].",";
				//echo $polygon[$i];echo "<br/>";
			}
			//echo $poly;
			//echo print_r($polygon);
			//exit;
			
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'id' 			=> $row->id,									
					'pkkecode' 		=> $row->pkkecode,									
					'nama_kec' 		=> $row->nama_kec								
					//'polygon' 			=> $row->polygon
				),
				'geometry' => array(
					'type' => 'LineString',
					'coordinates' => $poly
					
				)
			);
			array_push($geoJSON['features'], $marker);	
		}
		header('Content-type: application/json');
		$hasil = json_encode($geoJSON,JSON_NUMERIC_CHECK);
		$hasil = str_replace('"[','[[',$hasil);
		$hasil = str_replace(']"',']]',$hasil);
		echo $hasil;
		//echo json_encode(str_replace('"[','[',$geoJSON),JSON_NUMERIC_CHECK);
    }
	
	public function getCenterGeomKecamatan()
    {	
		$kec = "";
		if(ISSET($_GET['kec']))
			$kec = $_GET['kec'];
		
		$kecamatan = DB::select("SELECT id, pkkecode, nama_kec, ST_AsText(ST_centroid(ST_Transform(geom, 4326))) as point  FROM tb_kecamatan WHERE pkkecode='$kec'");
		foreach($kecamatan as $row){
			$points = str_replace("POINT(","",$row->point);
			$points = str_replace(")","",$points);
			$points = explode(" ",$points);
		}
		$latlon = array(
			'lat' => $points[0],	
			'lon' => $points[1]
		);
		echo json_encode($latlon);
	}
	
	public function getCenterGeomKelurahan()
    {	
		$kel = "";
		if(ISSET($_GET['kel']))
			$kel = $_GET['kel'];
		
		$kelurahan = DB::select("SELECT gid, admncode, desaname, ST_AsText(ST_centroid(ST_Transform(geom, 4326))) as point  FROM tb_desa WHERE admncode='$kel'");
		foreach($kelurahan as $row){
			$points = str_replace("POINT(","",$row->point);
			$points = str_replace(")","",$points);
			$points = explode(" ",$points);
			//$points =array($points);
		}
		//$lat = $points[0];
		//$lon = $points[1];
		$latlon = array(
			'lat' => $points[0],	
			'lon' => $points[1]
		);
		//echo $lat;echo $lon;
		//echo $points;
		//echo $points[0];
		//exit;
		echo json_encode($latlon);
		
		/*
		foreach($kelurahan as $row){
			$points = str_replace("POINT(","",$row->point);
			$points = str_replace(")","",$points);
			$points = explode(",",$points);
			foreach($points as $key => $val) {
				 $points[$key] = ltrim($points[$key]," ");
				 $points[$key] = "[" . $points[$key] . "]";
			}
			$points = str_replace(" ", "," ,$points);
			
			$point = "";
			for ($i=0;$i<count($points);$i++){
				if ($i==count($points)-1)
					$point = $point.$points[$i];
				else
					$point = $point.$points[$i].",";
			}
			//echo $point;exit;
			
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'id' 			=> $row->gid,									
					'admncode' 		=> $row->admncode,									
					'desaname' 		=> $row->desaname							
					//'point'			=> $row->point
				),
				'geometry' => array(
					'type' => 'Point',
					'coordinates' => $point					
				)
			);
			
			//array_push($geoJSON['features'], $marker);	
			echo json_encode($marker);
		}*/
	}
	
	public function getDataGISKelurahan()
    {	
		$kel = "";
		if(ISSET($_GET['kel'])){
			$kel = $_GET['kel'];
			$kelurahan = DB::select("SELECT gid, admncode, desaname, ST_AsText(ST_Transform(geom, 4326)) as polygon  FROM tb_desa WHERE admncode='$kel'");
		}
		else
			$kelurahan = DB::select("SELECT gid, admncode, desaname, ST_AsText(ST_Transform(geom, 4326)) as polygon  FROM tb_desa");
		
		$geoJSON = array(
			'type' => 'featureCollection',
			'features' => array()
		);
		
								
		foreach($kelurahan as $row){
			$polygon = str_replace("MULTIPOLYGON(((","",$row->polygon);
			$polygon = str_replace(")))","",$polygon);
			$polygon = explode(",",$polygon);
			foreach($polygon as $key => $val) {
				 $polygon[$key] = ltrim($polygon[$key]," ");
				 $polygon[$key] = "[" . $polygon[$key] . "]";
			}
			$polygon = str_replace(" ", "," ,$polygon);
			
			$poly = "";
			for ($i=0;$i<count($polygon);$i++){
				if ($i==count($polygon)-1)
					$poly = $poly.$polygon[$i];
				else
					$poly = $poly.$polygon[$i].",";
			}
			
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'id' 			=> $row->gid,									
					'admncode' 		=> $row->admncode,									
					'desaname' 		=> $row->desaname								
					//'polygon' 			=> $row->polygon
				),
				'geometry' => array(
					'type' => 'LineString',
					'coordinates' => $poly
					
				)
			);
			array_push($geoJSON['features'], $marker);	
		}
		header('Content-type: application/json');
		$hasil = json_encode($geoJSON,JSON_NUMERIC_CHECK);
		$hasil = str_replace('"[','[[',$hasil);
		$hasil = str_replace(']"',']]',$hasil);
		echo $hasil;
    }
	
	public function getDataGISJembatan()
    {		
		$geoJSON = array(
			'type' => 'featureCollection',
			'features' => array()
		);
		
		$jembatan = DB::select("SELECT nama_jalan, nama_jembatan, tb_jembatan.file_path, tb_jembatan.file_name, nama_kec as kecamatan, desaname as desa, panjang, lebar, x, y,
								kondisi,  tb_status_jembatan.file_path as icon_path, tb_status_jembatan.file_name as icon FROM tb_jembatan JOIN tb_kecamatan ON tb_jembatan.kecamatan=tb_kecamatan.pkkecode 
								JOIN tb_desa ON desa=admncode JOIN tb_kondisi_umum ON tb_jembatan.kondisi_umum=tb_kondisi_umum.id
								JOIN tb_status_jembatan ON tb_jembatan.status_jembatan = tb_status_jembatan.id
								WHERE tb_jembatan.published=1");
								
		foreach($jembatan as $row){
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'nama_jalan' 		=> $row->nama_jalan,									
					'nama_jembatan' 	=> $row->nama_jembatan,									
					'kecamatan' 		=> $row->kecamatan,
					'desa' 				=> $row->desa,
					'panjang' 			=> $row->panjang,
					'lebar' 			=> $row->lebar,
					'Koordinat_X' 		=> $row->x,
					'Koordinat_Y' 		=> $row->y,		
					'file_path' 		=> $row->file_path,
					'file_name'			=> $row->file_name,
					'kondisi_umum'		=> $row->kondisi,
					'Foto' 				=> $row->file_name,
					'icon_path'			=> $row->icon_path,
					'icon' 				=> $row->icon
				),
				'geometry' => array(
					'type' => 'Point',
					'coordinates' => array(
						$row->x,
						$row->y
					)
				)
			);
			array_push($geoJSON['features'], $marker);	
		}
		header('Content-type: application/json');
		echo json_encode($geoJSON,JSON_NUMERIC_CHECK);
		
		/*
		return Response::json([
			'type' => 'Feature',
			'properties' => $jembatan
		],200); */
    }
	
	public function getDataGISJembatanBackendFilter()
    {		
		$id_jalan = null;
		if(ISSET($_GET['id_jalan']))
			$id_jalan = $_GET['id_jalan'];
		
		$kecamatan = null;
		if(ISSET($_GET['kecamatan']))
			$kecamatan = $_GET['kecamatan'];
		
		$desa = null;
		if (ISSET($_GET['desa']))
			$desa = $_GET['desa'];
					
		$where = '';
		if ($id_jalan!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " id_jalan =  '".str_replace('%20',' ',$id_jalan)."'";
			}
			else
				$where .= " id_jalan =  '".str_replace('%20',' ',$id_jalan)."'";
			
		}
		if ($kecamatan!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " kecamatan =  '".str_replace('%20',' ',$kecamatan)."'";
			}
			else
				$where .= " AND kecamatan =  '".str_replace('%20',' ',$kecamatan)."'";
		}
		if ($desa!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " desa =  '".str_replace('%20',' ',$desa)."'";
			}
			else
				$where .= " AND desa =  '".str_replace('%20',' ',$desa)."'";
		}
		
		$geoJSON = array(
			'type' => 'featureCollection',
			'features' => array()
		);
		
		$jembatan = DB::select("SELECT nama_jalan,nama_jembatan, tb_jembatan.file_path, tb_jembatan.file_name, nama_kec as kecamatan, desaname as desa, panjang, lebar, x, y,
								kondisi, tb_status_jembatan.file_path as icon_path, tb_status_jembatan.file_name as icon FROM tb_jembatan JOIN tb_kecamatan ON tb_jembatan.kecamatan=tb_kecamatan.pkkecode 
								JOIN tb_desa ON desa=admncode JOIN tb_kondisi_umum ON tb_jembatan.kondisi_umum=tb_kondisi_umum.id
								LEFT JOIN tb_status_jembatan ON tb_jembatan.status_jembatan = tb_status_jembatan.id
								$where");
						
		foreach($jembatan as $row){
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'nama_jalan' 		=> $row->nama_jalan,
					'nama_jembatan' 	=> $row->nama_jembatan,									
					'kecamatan' 		=> $row->kecamatan,
					'desa' 				=> $row->desa,
					'panjang' 			=> $row->panjang,
					'lebar' 			=> $row->lebar,
					'Koordinat_X' 		=> $row->x,
					'Koordinat_Y' 		=> $row->y,							
					'file_path' 		=> $row->file_path,
					'file_name'			=> $row->file_name,
					'kondisi_umum'		=> $row->kondisi,
					'Foto' 				=> $row->file_name,
					'icon_path'			=> $row->icon_path,
					'icon' 				=> $row->icon
				),
				'geometry' => array(
					'type' => 'Point',
					'coordinates' => array(
						$row->x,
						$row->y
					)
				)
			);
			array_push($geoJSON['features'], $marker);	
		}
		header('Content-type: application/json');
		echo json_encode($geoJSON,JSON_NUMERIC_CHECK);
    }
	
	public function getDataGISJembatanFilter()
    {		
		$id_jalan = null;
		if(ISSET($_GET['id_jalan']))
			$id_jalan = $_GET['id_jalan'];
		
		$kecamatan = null;
		if(ISSET($_GET['kecamatan']))
			$kecamatan = $_GET['kecamatan'];
		
		$desa = null;
		if (ISSET($_GET['desa']))
			$desa = $_GET['desa'];
			
		$kondisi = null;
		if (ISSET($_GET['kondisi']))
			$kondisi = $_GET['kondisi'];
			
		$tahun_bangun = null;
		if (ISSET($_GET['tahun_bangun']))
			$tahun_bangun = $_GET['tahun_bangun'];
		
		$tahun_peresmian= null;
		//if (ISSET($_GET['tahun_peresmian']))
		//	$tahun_peresmian = $_GET['tahun_peresmian'];
		
		$tahun_penanganan = null;
		if (ISSET($_GET['tahun_penanganan']))
			$tahun_penanganan = $_GET['tahun_penanganan'];
		
		$where = '';
		if ($id_jalan!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " id_jalan =  '".str_replace('%20',' ',$id_jalan)."'";
			}
			else
				$where .= " id_jalan =  '".str_replace('%20',' ',$id_jalan)."'";
			
		}
		if ($kecamatan!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " kecamatan =  '".str_replace('%20',' ',$kecamatan)."'";
			}
			else
				$where .= " AND kecamatan =  '".str_replace('%20',' ',$kecamatan)."'";
		}
		if ($desa!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " desa =  '".str_replace('%20',' ',$desa)."'";
			}
			else
				$where .= " AND desa =  '".str_replace('%20',' ',$desa)."'";
		}
		
		if ($kondisi!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " kondisi_umum =  '".str_replace('%20',' ',$kondisi)."'";
			}
			else
				$where .= " AND kondisi_umum =  '".str_replace('%20',' ',$kondisi)."'";
		}
		
		if ($tahun_bangun!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " Extract(YEAR FROM tanggal_selesai)=  '".str_replace('%20',' ',$tahun_bangun)."'";
			}
			else
				$where .= " AND Extract(YEAR FROM tanggal_selesai) =  '".str_replace('%20',' ',$tahun_bangun)."'";
		}
		/*
		if ($tahun_peresmian!='null') {
			if ($where=='') {
				$where = " WHERE";
				$where .= " Extract(YEAR FROM tanggal_peresmian)=  '".str_replace('%20',' ',$tahun_peresmian)."'";
			}
			else
				$where .= " AND Extract(YEAR FROM tanggal_peresmian) =  '".str_replace('%20',' ',$tahun_peresmian)."'";
		} */

		if ($tahun_penanganan!='null') {
			if ($where=='') {
				$where = "  WHERE";
				$where .= " Extract(YEAR FROM tb_penanganan_jembatan.tanggal)=  '".str_replace('%20',' ',$tahun_penanganan)."'";
			}
			else
				$where .= " AND Extract(YEAR FROM tb_penanganan_jembatan.tanggal) =  '".str_replace('%20',' ',$tahun_penanganan)."'";
		}
		
		$geoJSON = array(
			'type' => 'featureCollection',
			'features' => array()
		);
		
		$sSQL = "SELECT distinct nama_jalan,nama_jembatan, tb_jembatan.file_path, tb_jembatan.file_name, nama_kec as kecamatan, desaname as desa, panjang, lebar, tb_jembatan.x, tb_jembatan.y,
				kondisi, tb_status_jembatan.file_path as icon_path, tb_status_jembatan.file_name as icon FROM tb_jembatan JOIN tb_kecamatan ON tb_jembatan.kecamatan=tb_kecamatan.pkkecode 
				JOIN tb_desa ON desa=admncode JOIN tb_kondisi_umum ON tb_jembatan.kondisi_umum=tb_kondisi_umum.id
				LEFT JOIN tb_penanganan_jembatan ON tb_jembatan.id = tb_penanganan_jembatan.id_jembatan
				LEFT JOIN tb_status_jembatan ON tb_jembatan.status_jembatan = tb_status_jembatan.id
				$where";
		//echo $sSQL;exit;
		$jembatan = DB::select($sSQL);
						
		foreach($jembatan as $row){
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'nama_jalan' 		=> $row->nama_jalan,
					'nama_jembatan' 	=> $row->nama_jembatan,									
					'kecamatan' 		=> $row->kecamatan,
					'desa' 				=> $row->desa,
					'panjang' 			=> $row->panjang,
					'lebar' 			=> $row->lebar,
					'Koordinat_X' 		=> $row->x,
					'Koordinat_Y' 		=> $row->y,							
					'file_path' 		=> $row->file_path,
					'file_name'			=> $row->file_name,
					'kondisi_umum'		=> $row->kondisi,
					'Foto' 				=> $row->file_name,
					'icon_path'			=> $row->icon_path,
					'icon' 				=> $row->icon
				),
				'geometry' => array(
					'type' => 'Point',
					'coordinates' => array(
						$row->x,
						$row->y
					)
				)
			);
			array_push($geoJSON['features'], $marker);	
		}
		header('Content-type: application/json');
		echo json_encode($geoJSON,JSON_NUMERIC_CHECK);
    }
	
	public function getDataGISPenangananJembatan($id)
    {		
		$geoJSON = array(
			'type' => 'featureCollection',
			'features' => array()
		);
		
		$jembatan = DB::select("SELECT nama_jalan, nama_jembatan, tb_jembatan.file_path, tb_jembatan.file_name, nama_kec as kecamatan, desaname as desa, panjang, lebar, x, y,
								kondisi,  tb_status_jembatan.file_path as icon_path, tb_status_jembatan.file_name as icon FROM tb_jembatan 
								JOIN tb_kecamatan ON tb_jembatan.kecamatan=tb_kecamatan.pkkecode 
								JOIN tb_desa ON desa=admncode JOIN tb_kondisi_umum ON tb_jembatan.kondisi_umum=tb_kondisi_umum.id
								JOIN tb_status_jembatan ON tb_jembatan.status_jembatan = tb_status_jembatan.id
								WHERE tb_jembatan.id=$id");
								//WHERE tb_jembatan.published=1");
								
		foreach($jembatan as $row){
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'nama_jalan' 		=> $row->nama_jalan,									
					'nama_jembatan' 	=> $row->nama_jembatan,									
					'kecamatan' 		=> $row->kecamatan,
					'desa' 				=> $row->desa,
					'panjang' 			=> $row->panjang,
					'lebar' 			=> $row->lebar,
					'Koordinat_X' 		=> $row->x,
					'Koordinat_Y' 		=> $row->y,		
					'file_path' 		=> $row->file_path,
					'file_name'			=> $row->file_name,
					'kondisi_umum'		=> $row->kondisi,
					'Foto' 				=> $row->file_name,
					'icon_path'			=> $row->icon_path,
					'icon' 				=> $row->icon
				),
				'geometry' => array(
					'type' => 'Point',
					'coordinates' => array(
						$row->x,
						$row->y
					)
				)
			);
			array_push($geoJSON['features'], $marker);	
		}
		header('Content-type: application/json');
		echo json_encode($geoJSON,JSON_NUMERIC_CHECK);
    }
	
	public function getDataGISStatusPenangananJembatan($id)
    {		
		$geoJSON = array(
			'type' => 'featureCollection',
			'features' => array()
		);
		
		$jembatan = DB::select("SELECT tb_penanganan_jembatan.id,nama_jembatan,nama_jalan,tb_kecamatan.nama_kec,tb_desa.desaname,tb_penanganan_jembatan.bagian,
								jenis_tindakan,tb_penanganan_jembatan.keterangan,tb_status_penanganan.status_penanganan,tb_penanganan_jembatan.published,
								tb_penanganan_jembatan.x, tb_penanganan_jembatan.y, tb_status_penanganan.file_path, tb_status_penanganan.file_name
								FROM tb_jembatan JOIN tb_kecamatan ON tb_kecamatan.pkkecode=tb_jembatan.kecamatan
								join tb_desa ON tb_desa.admncode = tb_jembatan.desa
								join tb_penanganan_jembatan ON tb_penanganan_jembatan.id_jembatan = tb_jembatan.id
								join tb_status_penanganan ON tb_penanganan_jembatan.status_penanganan=tb_status_penanganan.id
								join tb_tindakan_penanganan ON tb_tindakan_penanganan.id=tb_penanganan_jembatan.tindakan
								WHERE tb_status_penanganan.published=1 AND id_jembatan=$id");
								
		foreach($jembatan as $row){
			$marker = array(
				'type' => 'Feature',
				'properties' => array(
					'nama_jalan' 		=> $row->nama_jalan,									
					'nama_jembatan' 	=> $row->nama_jembatan,									
					'bagian' 			=> $row->bagian,
					'jenis_tindakan' 	=> $row->jenis_tindakan,
					'status_penanganan' => $row->status_penanganan,
					'keterangan' 		=> $row->keterangan,
					'Koordinat_X' 		=> $row->x,
					'Koordinat_Y' 		=> $row->y,		
					'file_path' 		=> $row->file_path,
					'file_name'			=> $row->file_name
				),
				'geometry' => array(
					'type' => 'Point',
					'coordinates' => array(
						$row->x,
						$row->y
					)
				)
			);
			array_push($geoJSON['features'], $marker);	
		}
		header('Content-type: application/json');
		echo json_encode($geoJSON,JSON_NUMERIC_CHECK);
    }
	
	//GET DATA JSON
	public function getDataJSONJembatan()
    {		
		$jembatan = DB::select("SELECT count(nama_jembatan) AS jumlah, nama_kec FROM tb_jembatan
								JOIN tb_kecamatan ON tb_kecamatan.pkkecode=tb_jembatan.kecamatan
								GROUP BY nama_kec");	

		$jumlah = array();
		foreach($jembatan as $row){
			$jumlah[] = $row->jumlah;	
		}

		$jmlh = "";
		for ($i=0;$i<count($jumlah);$i++){
			if ($i==count($jumlah)-1)
				$jmlh = $jmlh.$jumlah[$i];
			else
				$jmlh = $jmlh.$jumlah[$i].",";
		}
		$jmlh = "[".$jmlh."]";
		echo $jmlh;
    }
	
	public function getDataJSONKecamatan_Chart()
    {		
		$jembatan = DB::select("SELECT nama_kec FROM tb_kecamatan ORDER BY nama_kec");	

		$kec = array();
		foreach($jembatan as $row){
			$kec[] = str_replace("Kecamatan","",$row->nama_kec);	
		}

		$jmlh = "";
		for ($i=0;$i<count($kec);$i++){
			if ($i==count($kec)-1)
				$jmlh = $jmlh."'".$kec[$i]."'";
			else
				$jmlh = $jmlh."'".$kec[$i]."'".",";
		}
		//echo $jmlh;exit;
		$jmlh = "[".$jmlh."]";
		echo $jmlh;
    }
	
	public function getDataJSONStatusJembatan()
    {		
		/*
		$jembatan = DB::select("SELECT count(nama_jembatan) AS jumlah, nama_kec FROM tb_jembatan
								JOIN tb_kecamatan ON tb_kecamatan.pkkecode=tb_jembatan.kecamatan
								JOIN tb_status_jembatan ON tb_jembatan.status_jembatan=tb_status_jembatan.id
								WHERE tb_jembatan.status_jembatan=1
								GROUP BY nama_kec ORDER BY nama_kec");
				*/
						//->get()->toArray();	
		//$propinsi = array_column($jembatan,'count');
		//echo print_r($propinsi);exit;		
		//return response::json($jembatan);exit;
		
		$status_jembatan = DB::select("SELECT * FROM vJembatan_Status_Gabungan ORDER BY nama_kec");
		return Response::json(['data'=>$status_jembatan]);
    }
	
}