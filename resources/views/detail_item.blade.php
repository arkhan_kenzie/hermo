	
	<div class="row">
		@foreach($best_selling['items'] as $data)
			<?php
				$path =  url($data['images']); 
			?>
			<div class="col-sm-4 col-lg-4 col-md-4">
				<div class="thumbnail">
					<img src="{{ $path }}" alt="" height="30" width="40">
					<div class="caption">
						<p class="text-center"><b>{{ $data['brand']['name'] }}</b></p>
						<p class="text-center">{{ $data['name'] }}</p>
						<p class="text-center">{{ $data['symbol'] }} &nbsp; {{ $data['selling_price'] }}</p>
						<p class="text-center"><strike>{{ $data['symbol'] }} &nbsp; {{ $data['retail_price'] }}</strike></p>
					</div>
					<div class="ratings">
						<p class="text-center">
							<select width="40">
								<option value="">Qty</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							<button type="button" class="btn btn-xs btn-default" id="btnPurchase">Buy Now</button>
						</p>
					</div>
				</div>
			</div>
		@endforeach
	</div>