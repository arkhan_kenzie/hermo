<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hermo</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('asset/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('asset/css/shop-homepage.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Page Content -->
	<form name='myForm' method="POST" action="">
    <div class="container">
        <div class="row">
			Select the receiver's country :&nbsp;&nbsp; {{ $receiver }}
			<div>	
				<img src="{{ url($images) }}" alt="" height="160" width="100">
			</div>
			<div>	
				{{ $name }}</td>
			</div>	
			<div>
				Unit Price : {{ $symbol }} {{ $selling_price }} <br />
				Qty : {{ $qty }}<br />
				Total Price : {{ $amount }}<br />
				Promotion Code : {{ $promotion_code }}<br />
				Discount : {{ $symbol }} {{ $discount }} <br />
				Delivery to : {{ $receiver }}<br />
				Shipping Fee : {{ $symbol }} {{ $shipping_fee }}<br />
				Payment Required : {{ $symbol }} {{ $payment }}<br />
			</div>
        </div>

    </div>
    <!-- /.container -->
	</form >

    <div class="container">
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Testing Hermo 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="{{ asset('asset/js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>

</body>
<script>
	$(document).ready(function(){
		
	});
</script>	
</html>
