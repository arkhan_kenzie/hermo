<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hermo</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('asset/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('asset/css/shop-homepage.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Page Content -->
	<form name='myForm' method="POST" action="{{ url('backend/order') }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="container">
        <div class="row">
			Select the receiver's country &nbsp;&nbsp;
			<select name="receiver" id="receiver">
				<option value="Malaysia">Malaysia</option>
				<option value="Singapore">Singapore</option>
				<option value="Brunai">Brunei</option>
			</select><br /><br />
			
			<table class="table table-bordered table-striped">
			<th>Items</th>
			<th>Description</th>
			<th>Unit Price</th>
			<th>Qty</th>
			<th>Total</th>
			<tr>
				<?php 
					$qty = $_POST['qty'];
					$amount = $qty * $selling_price;
				?>
				<td><img src="{{ url($images) }}" alt="" height="160" width="100"><input type="hidden" name="images" value="{{ $images }}" ><input type="hidden" name="brand" value="{{ $brand }}" ></td>
				<td>{{ $name }}<input type="hidden" class="form-control" name="name" value="{{ $name }}" ></td>
				<td>{{ $symbol }} {{ $selling_price }} <br /><strike>{{ $symbol }}{{ $retail_price }}</strike><input type="hidden" name="retail_price" value="{{ $retail_price }}" ></td>
				<td>{{ $qty }}<input type="hidden" name="symbol" value="{{ $symbol }}"><input type="hidden" class="form-control" name="qty" value="{{ $qty }}" ></td>
				<td>{{ $amount }}<input type="hidden" class="form-control" name="selling_price" value="{{ $selling_price }}" ><input type="hidden" class="form-control" name="amount" value="{{ $amount }}" ></td>
			<tr>
			</table><br >
			<label class="col-md-3 control-label">Promotion Code</label>
			<div class="col-md-3">
				<input type="text" class="form-control" name="promotion_code" >
			</div>
			<button type="submit" class="btn btn-primary">Confirm Checkout</button>
        </div>

    </div>
    <!-- /.container -->
	</form >

    <div class="container">
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Testing Hermo 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="{{ asset('asset/js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>

</body>
<script>
	$(document).ready(function(){
		
	});
</script>	
</html>
